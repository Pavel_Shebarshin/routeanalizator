package com.routeananalysis.analysis;

import com.routeananalysis.structures.Point;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class AnalysisLogicTest {
    @Test
    void normalizeTest() {
        List<Point> standardPoints = new ArrayList<>();
        List<Point> inspectedPoints = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            standardPoints.add(new Point(500 + i, 500 + i * 2, 500));
            inspectedPoints.add(new Point(500 - i, 500 + i, 500));
        }
        AnalysisLogic.normalize(standardPoints, inspectedPoints);
        for (int i = 0; i < standardPoints.size(); i++) {
            Assertions.assertNotEquals(500, standardPoints.get(i).getZ());
            Assertions.assertNotEquals(500, inspectedPoints.get(i).getZ());
        }
        Assertions.assertEquals(0, inspectedPoints.get(inspectedPoints.size() - 1).getX());
    }
}
