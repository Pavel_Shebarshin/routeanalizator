package com.routeananalysis.structures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PointTest {
    @Test
    void getTest() {
        Point point = new Point(1, 2, 3, 4);
        Assertions.assertEquals(1, point.getX());
        Assertions.assertEquals(2, point.getY());
        Assertions.assertEquals(3, point.getZ());
        Assertions.assertEquals(4, point.getSpeed());
    }

    @Test
    void getValueTest() {
        Point point = new Point(22, 33, 44);
        Assertions.assertEquals(22, point.getValue('X'));
        Assertions.assertEquals(33, point.getValue('Y'));
        Assertions.assertEquals(44, point.getValue('Z'));
    }
}
