package com.routeananalysis.structures;

import javafx.util.Pair;

import java.util.List;

public class AnalysisResult {
    private final boolean isEquals;
    private final List<Point> standardPoints;
    private final List<Point> inspectedPoints;
    private final List<Pair<Point, Boolean>> pointsToPrint;

    public AnalysisResult(boolean isEquals, List<Point> standardPoints,
                          List<Point> inspectedPoints, List<Pair<Point, Boolean>> pointsToPrint) {
        this.isEquals = isEquals;
        this.standardPoints = standardPoints;
        this.inspectedPoints = inspectedPoints;
        this.pointsToPrint = pointsToPrint;
    }

    public List<Point> getStandardPoints() {
        return standardPoints;
    }

    public List<Point> getInspectedPoints() {
        return inspectedPoints;
    }

    public boolean isEquals() {
        return isEquals;
    }

    public List<Pair<Point, Boolean>> getPointsToPrint() {
        return pointsToPrint;
    }
}
