package com.routeananalysis.console;

import com.routeananalysis.analysis.RoutesAnalyzer;

import java.io.File;
import java.util.logging.Logger;

public class ConsoleAnalyser {

    private static final Logger LOGGER = Logger.getLogger(ConsoleAnalyser.class.getName());

    public static void main(String[] args) {
        RoutesAnalyzer routesAnalyzer = new RoutesAnalyzer(new File(args[0]), new File(args[1]));
        LOGGER.info(() -> String.valueOf(routesAnalyzer.analyze().isEquals()));
    }
}
