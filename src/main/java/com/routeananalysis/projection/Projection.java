package com.routeananalysis.projection;

/**
 * This interface represents mapping of surface coordinates to flat Cartesian coordinates.
 */
public interface Projection {

    /**
     * This method should return array of four doubles; the first pair must correspond
     * to coordinates of the lower left point of com.routeanalyzer.projection rectangle; the second pair corresponds to
     * the upper right corner in the same way. If com.routeanalyzer.projection has non-rectangular image,
     * then UnsupportedOperationException should be thrown.
     *
     * @return array of four coordinates (two per point); the first is lower left point of the com.routeanalyzer.projection rectangle;
     * the second is upper right point of the rectangle
     */
    double[] getImageBounds();

    /**
     * Checks whether given point is an image of com.routeanalyzer.projection (e. g. fits in the bounds returned by getBounds()).
     *
     * @param abscissa - abscissa of the point to check
     * @param ordinate - ordinate of the point to check
     * @return true if and only if abscissa and ordinate being given may be transformed back
     * to surface coordinates (if they fit the bounds)
     */
    boolean isImage(double abscissa, double ordinate);

    /**
     * Checks whether given point is a preimage of the com.routeanalyzer.projection and thus may be projected.
     * Some projections limit the preimage only to a particular subset of the surface in order to avoid
     * calculations in the neighbourhood of singularity.
     *
     * @param u - first surface coordinate of the point
     * @param v - second surface coordinate of the point
     * @return true if and only if the (u, v) point can be projected to the plane
     */
    boolean isPreimage(double u, double v);

    /**
     * Projects the given surface point to the com.routeanalyzer.projection plane.
     *
     * @param u - first surface coordinate of point to project
     * @param v - second surface coordinate of point to project
     * @throws IllegalArgumentException if the point could not be projected
     */
    double[] project(double u, double v);

    /**
     * Returns surface coordinates of the point being given its com.routeanalyzer.projection.
     *
     * @param abscissa - abscissa of the com.routeanalyzer.projection
     * @param ordinate - ordinate of the com.routeanalyzer.projection
     * @return array of two doubles standing for the first and the second surface coordinates
     * of the preimage of com.routeanalyzer.projection;
     * @throws IllegalArgumentException if the projected point given has no preimage
     */
    double[] inverse(double abscissa, double ordinate) throws IllegalArgumentException;

    /**
     * In case the com.routeanalyzer.projection is conformal, this method returns its scale factor corresponding to the
     * given point in curvilinear coordinates (ratio between length of com.routeanalyzer.projection and original length).
     * If com.routeanalyzer.projection is non-conformal then an UnsupportedOperationException should be thrown.
     *
     * @return scale factor for the given point of the surface; formally, it gives ratio between
     * norms of tangent vectors of com.routeanalyzer.projection and original surface; thus it may be used as a valid
     * scale factor for sufficiently small distances in neighbourhood of the location passed;
     * @throws IllegalArgumentException if pair (u, v) is not a preimage of the com.routeanalyzer.projection
     */
    double scale(double u, double v);

    /**
     * Reports if the com.routeanalyzer.projection is conformal (preserves angles, has isotropic scale).
     *
     * @return true if and only if the com.routeanalyzer.projection is conformal; that means, angles
     * on the surface are exactly matched in com.routeanalyzer.projection, and scale() function should
     * return isotropic scale for given point of the surface instead of throwing an exception
     */
    boolean isConformal();
}
