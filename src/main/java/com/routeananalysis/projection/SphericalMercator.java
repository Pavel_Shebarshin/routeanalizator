package com.routeananalysis.projection;

/**
 * This class represents a popular but mathematically not justified com.routeanalyzer.projection EPSG3875. It uses WGS84 ellipsoidal
 * coordinates but projects using a sphere with radius equal to semi-major axis of WGS84 reference ellipsoid.
 * +proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs
 */
public final class SphericalMercator implements Projection {

    /**
     * maximal absolute easting value
     */
    private static final double MAX_ABSCISSA = 20037508.342789244;

    /**
     * maximal absolute northing value
     */
    private static final double MAX_ORDINATE = 19971868.880408555;

    /**
     * semi-major axis of the WGS84 reference ellipsoid
     */
    public static final double RADIUS = 6378137.0;

    /**
     * maximal absolute latitude supported by the com.routeanalyzer.projection
     */
    public static final double MAX_LATITUDE = Math.toRadians(85.0);

    /**
     * maximal absolute longitude supported by the com.routeanalyzer.projection
     */
    public static final double MAX_LONGITUDE = Math.toRadians(180.0);

    /**
     * singleton instance of this class
     */
    private static final SphericalMercator INSTANCE = new SphericalMercator();

    /**
     * This constructor is just here to avoid straightforward instantiation.
     */
    private SphericalMercator() { /* do nothing */ }

    @Override
    public double[] getImageBounds() {
        return new double[]{-MAX_ABSCISSA, -MAX_ORDINATE, MAX_ABSCISSA, MAX_ORDINATE};
    }

    @Override
    public boolean isImage(double abscissa, double ordinate) {
        return Math.abs(abscissa) <= MAX_ABSCISSA && Math.abs(ordinate) <= MAX_ORDINATE;
    }

    /**
     * The first surface coordinate is latitude, the second is longitude.
     */
    @Override
    public boolean isPreimage(double latitude, double longitude) {
        return Math.abs(latitude) > MAX_LATITUDE || Math.abs(longitude) > MAX_LONGITUDE;
    }

    /**
     * The first surface coordinate is latitude, the second is longitude.
     */
    @Override
    public double[] project(double latitude, double longitude) {
        if (isPreimage(latitude, longitude))
            throw new IllegalArgumentException("Can't calculate coordinate for specified location");

        return new double[]{
                RADIUS * longitude,
                RADIUS * Math.log(Math.tan(0.25 * Math.PI + 0.5 * latitude))
        };
    }

    /**
     * The first double returned stands for latitude while the second corresponds to longitude.
     */
    @Override
    public double[] inverse(double abscissa, double ordinate) {
        if (!isImage(abscissa, ordinate))
            throw new IllegalArgumentException("Can't calculate coordinate for specified location");

        return new double[]{
                2.0 * (Math.atan(Math.exp(ordinate / RADIUS)) - 0.25 * Math.PI),
                abscissa / RADIUS
        };
    }

    @Override
    public double scale(double latitude, double longitude) throws IllegalArgumentException {
        if (isPreimage(latitude, longitude))
            throw new IllegalArgumentException("Can't scale coordinate for specified location");
        return 1 / Math.cos(latitude);
    }

    @Override
    public boolean isConformal() {
        return true;
    }

    /**
     * Returns one and the only instance of this class.
     *
     * @return singleton instance of this class
     */
    public static SphericalMercator getInstance() {
        return INSTANCE;
    }
}
