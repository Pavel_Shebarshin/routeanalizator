package com.routeananalysis.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class GUI extends Application {

    public static Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        URL path = getClass().getResource("/mainGUI.fxml");
        assert path != null;
        Parent root = FXMLLoader.load(path);
        primaryStage.setTitle("TelemetryComparer");
        primaryStage.setScene(new Scene(root, primaryStage.getMaxWidth(), primaryStage.getMaxHeight()));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
