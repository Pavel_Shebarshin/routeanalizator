package com.routeananalysis.gui;

import com.routeananalysis.analysis.AnalysisLogic;
import com.routeananalysis.analysis.RoutesAnalyzer;
import com.routeananalysis.structures.AnalysisResult;
import com.routeananalysis.structures.Point;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

public class Controller {

    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    private final FileChooser fileChooser = new FileChooser();

    private File file1;
    private File file2;

//    @FXML
//    private Label valuesFromFirstFile;
//    @FXML
//    private Label sliderValue;
//    @FXML
//    private Slider slider;

    @FXML
    public Canvas routeCanvas;
    @FXML
    public Canvas lineCanvas;

    @FXML
    private void initialize() {
//        slider.setMin(1);
//        slider.setMax(1);
//        valuesFromFirstFile.setText("Latitude:\nLongitude:\nAltitude:");
//        slider.valueProperty().addListener((obs, oldVal, newVal) -> slider.setValue(newVal.intValue()));
//        sliderValue.textProperty().bind(Bindings.format("%.0f", slider.valueProperty()));
//        slider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
//            valuesFromFirstFile.textProperty().setValue(
//                    "Latitude: " + file1.get((int) slider.getValue()).getX() + '\n'
//                            + "Longitude: " + file1.get((int) slider.getValue()).getY() + '\n'
//                            + "Altitude: " + file1.get((int) slider.getValue()).getZ());
//        });
    }

    public void onClickButtonReadFile1(MouseEvent mouseEvent) {
        file1 = fileChooser.showOpenDialog(GUI.stage);
//        file1 = new File("C:\\Для рабочего стола\\1.csv");
    }

    public void onClickButtonReadFile2(MouseEvent mouseEvent) {
        file2 = fileChooser.showOpenDialog(GUI.stage);
//        file2 = new File("C:\\Для рабочего стола\\Ethalon.csv");
    }

    public void checkRoutes(MouseEvent mouseEvent) {
        if (checkFilesForNull()) return;
        RoutesAnalyzer comparator = new RoutesAnalyzer(file1, file2);
        AnalysisResult analysisResult = comparator.analyze();
        LOGGER.info(() -> String.valueOf(analysisResult.isEquals()));
        List<Point> standardPoints = analysisResult.getStandardPoints();
        List<Point> inspectedPoints = analysisResult.getInspectedPoints();
        AnalysisLogic.normalize(standardPoints, inspectedPoints);

        GraphicsContext gc = routeCanvas.getGraphicsContext2D();
        gc.setLineWidth(5.0);
        gc.setStroke(Paint.valueOf("GREEN"));

        printRoutes(standardPoints, inspectedPoints, gc);

        GraphicsContext gcLine = lineCanvas.getGraphicsContext2D();
        comparator.printLine(gcLine, analysisResult.getPointsToPrint());
    }

    private void printRoutes(List<Point> standardPoints, List<Point> inspectedPoints, GraphicsContext gc) {
        gc.clearRect(0.0, 0.0, routeCanvas.getWidth(), routeCanvas.getHeight());
        for (int i = 1; i < standardPoints.size(); i++) {
            gc.strokeLine(standardPoints.get(i - 1).getX(), standardPoints.get(i - 1).getY(),
                    standardPoints.get(i).getX(), standardPoints.get(i).getY());
        }

        gc.setStroke(Paint.valueOf("RED"));
        gc.setLineWidth(2.0);
        for (int i = 1; i < inspectedPoints.size(); i++) {
            gc.strokeLine(inspectedPoints.get(i - 1).getX(), inspectedPoints.get(i - 1).getY(),
                    inspectedPoints.get(i).getX(), inspectedPoints.get(i).getY());
        }
    }

    private boolean checkFilesForNull() {
        if (file1 == null || file2 == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Routes problem");
            alert.setHeaderText("Not all routes are selected");
            alert.setContentText("You have to select all the routes");
            alert.showAndWait();
            return true;
        }
        return false;
    }

    public static void showExceptionMessage(Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(e.getClass().getName());
        alert.setHeaderText(e.getClass().getName());
        alert.setContentText(e.getMessage());
        alert.showAndWait();
    }
}
