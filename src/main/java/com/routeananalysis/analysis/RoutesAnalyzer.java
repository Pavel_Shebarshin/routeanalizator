package com.routeananalysis.analysis;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.routeananalysis.gui.Controller;
import com.routeananalysis.structures.AnalysisResult;
import com.routeananalysis.structures.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;
import javafx.util.Pair;
import com.routeananalysis.projection.SphericalMercator;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RoutesAnalyzer {

    private final List<Point> standardPoints;
    private final List<Point> inspectedPoints;

    private int latitudeIndex = -1;
    private int longitudeIndex = -1;
    private int altitudeIndex = -1;
    private int groundSpeedIndex = -1;
    private int commandIndex = -1;

    public RoutesAnalyzer(File standardFile, File inspectedFile) {
        standardPoints = readDataFromFile(standardFile);
        inspectedPoints = readDataFromFile(inspectedFile);
    }

    private List<Point> readDataFromFile(File file) {
        CSVParser csvParser = new CSVParserBuilder().withSeparator(',').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(file)).withCSVParser(csvParser).build()) {
            List<String[]> dataFromFile = reader.readAll();
            List<Point> points = new ArrayList<>();
            checkFile(dataFromFile);
            dataFromFile.remove(0);
            for (String[] strings : dataFromFile) {
                if (strings[commandIndex].equals("land_command") || strings[commandIndex].equals("Upload")
                        || strings[commandIndex].equals("") || strings[commandIndex].equals("disarm")) {
                    continue;
                }
                points.add(new Point(
                        Double.parseDouble(strings[latitudeIndex]),
                        Double.parseDouble(strings[longitudeIndex]),
                        Double.parseDouble(strings[altitudeIndex]),
                        Double.parseDouble(strings[groundSpeedIndex])
                ));
            }
            return points;
        } catch (IOException | CsvException e) {
            Controller.showExceptionMessage(e);
        }
        throw new IllegalArgumentException("Incorrect file name");
    }

    private void checkFile(List<String[]> dataFromFile) {
        if (altitudeIndex == -1 && longitudeIndex == -1 && latitudeIndex == -1 && commandIndex == -1
                && groundSpeedIndex == -1) {
            findParametersIndexes(dataFromFile);
        }
        if (altitudeIndex == -1 || longitudeIndex == -1 || latitudeIndex == -1 || commandIndex == -1
                || groundSpeedIndex == -1) {
            throw new IllegalArgumentException("There are no necessary columns in file");
        }
    }

    private void findParametersIndexes(List<String[]> dataFromFile) {
        for (int i = 0; i < dataFromFile.get(0).length; i++) {
            switch (dataFromFile.get(0)[i]) {
                case "latitude":
                    latitudeIndex = i;
                    break;
                case "longitude":
                    longitudeIndex = i;
                    break;
                case "control_server:altitude_amsl":
                    altitudeIndex = i;
                    break;
                case "ground_speed":
                    groundSpeedIndex = i;
                    break;
                case "control_server:command":
                    commandIndex = i;
                    break;
                default:
                    break;
            }
        }
    }

    public AnalysisResult analyze() {
        AnalysisLogic.convert(standardPoints, inspectedPoints);
        List<Pair<Point2D, Point2D>> segments = new ArrayList<>();
        List<Point2D> centers = new ArrayList<>();
        double radius = 1.0;

        SphericalMercator sphericalMercator = SphericalMercator.getInstance();
        double[] inverse = sphericalMercator.inverse(standardPoints.get(200).getX(), standardPoints.get(200).getY());
        double scaledRadius = radius * sphericalMercator.scale(inverse[0], inverse[1]);

        AnalysisResult analysisResult = checkRoutes(segments, centers, scaledRadius, standardPoints, inspectedPoints);

        AnalysisLogic.printLog(analysisResult);

        return analysisResult;
    }

    private AnalysisResult checkRoutes(List<Pair<Point2D, Point2D>> segments, List<Point2D> centers,
                                       double scaledRadius, List<Point> standardPoints, List<Point> inspectedPoints) {
        int size = Math.max(standardPoints.size(), inspectedPoints.size());
        List<Pair<Point, Boolean>> pointsToPrint = new ArrayList<>(size);
        List<Boolean> pointsCheck = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            pointsCheck.add(true);
        }
        boolean resultOfCheck = AnalysisLogic.checkRouteToOneSide(segments, centers, scaledRadius,
                standardPoints, inspectedPoints, pointsCheck, true);
        resultOfCheck = AnalysisLogic.checkRouteToOneSide(segments, centers, scaledRadius,
                inspectedPoints, standardPoints, pointsCheck, resultOfCheck);

        resultOfCheck = AnalysisLogic.checkRouteSpeed(standardPoints, inspectedPoints,
                pointsToPrint, pointsCheck, resultOfCheck);
        return new AnalysisResult(resultOfCheck, this.standardPoints, this.inspectedPoints, pointsToPrint);
    }

    public void printLine(GraphicsContext gc, List<Pair<Point, Boolean>> pointsToPrint) {
        gc.setLineWidth(10.0);
        int x = 0;
        for (Pair<Point, Boolean> point : pointsToPrint) {
            if (Boolean.TRUE.equals(point.getValue())) {
                gc.setStroke(Paint.valueOf("GREEN"));
            } else {
                gc.setStroke(Paint.valueOf("RED"));
            }
            gc.strokeLine(x, 0, ++x, 0);
        }
    }
}
