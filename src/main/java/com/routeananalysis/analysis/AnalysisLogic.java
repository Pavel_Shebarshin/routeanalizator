package com.routeananalysis.analysis;

import com.routeananalysis.projection.SphericalMercator;
import com.routeananalysis.structures.AnalysisResult;
import com.routeananalysis.structures.Point;
import javafx.util.Pair;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class AnalysisLogic {

    private static final Logger LOGGER = Logger.getLogger(AnalysisLogic.class.getName());

    private static final double SPEED_EPSILON = 5;
    private static final int SEPARATOR = 5;

    private AnalysisLogic() {
    }

    public static void normalize(List<Point> standardPoints, List<Point> inspectedPoints) {
        double minValueX = getMinimalValueOnAxis(standardPoints, inspectedPoints, 'X');
        double minValueY = getMinimalValueOnAxis(standardPoints, inspectedPoints, 'Y');
        double minValueZ = getMinimalValueOnAxis(standardPoints, inspectedPoints, 'Z');
        normalizeRoute(standardPoints, minValueX, minValueY, minValueZ);
        normalizeRoute(inspectedPoints, minValueX, minValueY, minValueZ);
    }

    private static void normalizeRoute(List<Point> points, double minValueX, double minValueY, double minValueZ) {
        for (int i = 0; i < points.size(); i++) {
            points.set(i, new Point(
                            points.get(i).getX() - minValueX,
                            points.get(i).getY() - minValueY,
                            points.get(i).getZ() - minValueZ,
                            points.get(i).getSpeed()
                    )
            );
        }
    }

    private static double getMinimalValueOnAxis(List<Point> standardPoints, List<Point> inspectedPoints, char axis) {
        double minValue = Double.MAX_VALUE;
        for (Point point : standardPoints) {
            if (minValue > point.getValue(axis)) {
                minValue = point.getValue(axis);
            }
        }
        for (Point point : inspectedPoints) {
            if (minValue > point.getValue(axis)) {
                minValue = point.getValue(axis);
            }
        }
        return minValue;
    }

    public static boolean isIntersects(double radius, Point2D center, Point2D point1, Point2D point2) {
        Point2D.Double p1 = new Point2D.Double(point1.getX() - center.getX(), point1.getY() - center.getY());
        Point2D.Double p2 = new Point2D.Double(point2.getX() - center.getX(), point2.getY() - center.getY());
        double a = p1.getY() - p2.getY();
        double b = p2.getX() - p1.getX();
        double c = p1.getX() * p2.getY() - p2.getX() * p1.getY();
        double x0 = -a * c / (a * a + b * b), y0 = -b * c / (a * a + b * b);
        double epsilon = 1E-6;
        double temp = radius * radius * (a * a + b * b);
        if (c * c > temp + epsilon) {
            return false;
        } else if (Math.abs(c * c - temp) < epsilon) {
            return x0 >= Math.min(p1.getX(), p2.getX()) && x0 <= Math.max(p1.getX(), p2.getX());
        } else {
            double d = radius * radius - c * c / (a * a + b * b);
            double ratio = Math.sqrt(d / (a * a + b * b));
            double ax
//                    , ay
                    , bx
//                    , by
                    ;
            ax = x0 + b * ratio;
            bx = x0 - b * ratio;
//            ay = y0 - a * ratio;
//            by = y0 + a * ratio;
            return (ax >= Math.min(p1.getX(), p2.getX()) && ax <= Math.max(p1.getX(), p2.getX()))
                    || (bx >= Math.min(p1.getX(), p2.getX()) && bx <= Math.max(p1.getX(), p2.getX()));
        }
    }

    public static void convert(List<Point> standardPoints, List<Point> inspectedPoints) {
        SphericalMercator sphericalMercator = SphericalMercator.getInstance();
        getConvert(sphericalMercator, standardPoints);
        getConvert(sphericalMercator, inspectedPoints);
    }

    private static void getConvert(SphericalMercator sphericalMercator, List<Point> points) {
        double[] projection;
        for (int i = 0; i < points.size(); i++) {
            projection = sphericalMercator.project(Math.toRadians(points.get(i).getX()),
                    Math.toRadians(points.get(i).getY()));
            points.set(i, new Point(projection[0], projection[1], points.get(i).getZ(), points.get(i).getSpeed()));
        }
    }

    public static boolean checkRouteSpeed(List<Point> standardPoints, List<Point> inspectedPoints,
                                          List<Pair<Point, Boolean>> pointsToPrint, List<Boolean> pointsCheck,
                                          boolean resultOfCheck) {
        int i = 0;
        for (Point point1 : standardPoints) {
            Point pointer = new Point(0, 0, 0, 0);
            double distance = Double.MAX_VALUE;
            for (Point point2 : inspectedPoints) {
                if (point1.get3DDistance(point1) < distance) {
                    pointer = point2;
                    distance = point1.get3DDistance(point1);
                }
            }
            boolean isBadSpeed = Math.abs(pointer.getSpeed() - point1.getSpeed()) > SPEED_EPSILON;
            int index = i / SEPARATOR;
            if (i % SEPARATOR == 0) {
                if (isBadSpeed) {
                    pointsToPrint.add(new Pair<>(standardPoints.get(index), false));
                } else {
                    pointsToPrint.add(new Pair<>(standardPoints.get(index), pointsCheck.get(index)));
                }
            }
            i++;
            if (isBadSpeed) {
                resultOfCheck = false;
            }
        }
        return resultOfCheck;
    }

    public static Boolean checkRouteToOneSide(List<Pair<Point2D, Point2D>> segments, List<Point2D> centers,
                                              double scaledRadius, List<Point> standardPoints, List<Point> inspectedPoints,
                                              List<Boolean> pointsCheck, boolean resultOfCheck) {
        int i = 0;
        checkLists(segments, centers, standardPoints, inspectedPoints);
        for (Point2D center : centers) {
            boolean isIntersects = false;
            for (Pair<Point2D, Point2D> segment : segments) {
                if (AnalysisLogic.isIntersects(scaledRadius, center, segment.getKey(), segment.getValue())) {
                    isIntersects = true;
                    break;
                }
            }
            if (!isIntersects) {
                resultOfCheck = false;
            }
            int index = i / SEPARATOR;
            if (i % SEPARATOR == 0) {
                if (pointsCheck.get(index) != isIntersects && isIntersects != true) {
                    pointsCheck.set(index, isIntersects);
                }
            }
            i++;
        }
        return resultOfCheck;
    }

    private static void checkLists(List<Pair<Point2D, Point2D>> segments, List<Point2D> centers,
                                   List<Point> standardPoints, List<Point> inspectedPoints) {
        for (Point point : standardPoints) {
            centers.add(new Point2D.Double(point.getX(), point.getY()));
        }
        for (int i = 0; i < inspectedPoints.size() - 1; i++) {
            segments.add(new Pair<>(new Point2D.Double(inspectedPoints.get(i).getX(), inspectedPoints.get(i).getY()),
                    new Point2D.Double(inspectedPoints.get(i + 1).getX(), inspectedPoints.get(i + 1).getY())));
        }
    }

    public static void printLog(AnalysisResult analysisResult) {
        if (!analysisResult.isEquals()) {
            boolean isBegin = true;
            StringBuilder log = new StringBuilder();
            for (int i = 0; i < analysisResult.getPointsToPrint().size(); i++) {
                if (isBegin && analysisResult.getPointsToPrint().get(i).getValue()) {
                    continue;
                } else if (isBegin && !analysisResult.getPointsToPrint().get(i).getValue()) {
                    log.append("Discrepancies are observed on [").
                            append(((float) i) / analysisResult.getPointsToPrint().size());
                    isBegin = false;
                } else if (!isBegin && analysisResult.getPointsToPrint().get(i).getValue()) {
                    log.append(", ").append(((float) i) / analysisResult.getPointsToPrint().size()).append("]\n");
                    isBegin = true;
                } else if (!isBegin && i == analysisResult.getPointsToPrint().size() - 1) {
                    log.append(", ").append(((float) i + 1) / analysisResult.getPointsToPrint().size()).append("]\n");
                }
            }
            LOGGER.log(Level.INFO, () -> '\n' + log.toString());
        }
    }
}
