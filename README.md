# README #

This program is designed to compare the telemetry of different versions of the autopilot. It can be used as a console application, or as a GUI application. 
### How to run ? ###

In order to run the application in IDEA, you first need to download JavaFX.
Then you need to read how to connect JavaFX in IDEA by following the link https://www.jetbrains.com/help/idea/javafx.html#vm-options
Well, at the end, in IDEA, you need to choose which application you want to run, console or GUI.
### How to work ? ###

If you have chosen a console application, you need to pass two CSV files to the command-line arguments: the standard and the inspected one.
If you have launched a GUI application, you need to select the files for comparison and then click on the "Check" button.

### Verification criteria ###
At the moment, only the difference in the speed and coordinate of the telemetries is checked.